from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import ToDoListForm, ToDoItemForm

# Create your views here.
def todo_list_list(request):
    name = TodoList.objects.all()
    context = {
        "todo_list_list": name,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    name = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": name,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = ToDoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = ToDoListForm()
        context = {
            "form": form,
        }
    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    name = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ToDoListForm(request.POST, instance=name)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = ToDoListForm(instance=name)
    context = {
        "post_form": form,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ToDoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ToDoItemForm()
        context = {
            "form": form,
        }
    return render(request, "todos/item_create.html", context)

#Below is feature 13

def todo_item_update(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ToDoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ToDoItemForm(instance=item)
    context = {
        "post_form": form,
        }
    return render(request, "todos/update.html", context)
