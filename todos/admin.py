from django.contrib import admin
from todos.models import TodoList
from todos.models import TodoItem

# Register your models here.
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    models_display = [
            "name",
            "id",
        ]

@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = [
            "task",
            "due_date",
            "is_completed",
    ]
